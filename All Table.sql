DROP TABLE  Student CASCADE CONSTRAINTS;
DROP TABLE  Course CASCADE CONSTRAINTS;
DROP TABLE  Teacher CASCADE CONSTRAINTS;
DROP TABLE  ENROLLED1 CASCADE CONSTRAINTS;
DROP TABLE  ENROLLED2 CASCADE CONSTRAINTS;

create table Student
(
	sId number, 
	sName varchar2(50), 
	SAddress varchar2(50), 
	sPhone varchar2(50),
	sEmail varchar2(50),
	sDateofBirth varchar2(50));

create table Course
(
	cId number, 
	cName varchar2(30),
	courseNumber NUMBER,
	Credit float, 
	TotalClass number, 
	tId number);

create table Teacher
(
	tId number, 
	tName varchar2(30), 
	tPhone NUMBER, 
	tEmail varchar2(50));

create table ENROLLED1
(
	sId number, 
	cID NUMBER,
	STATUS varchar2(15));

create table ENROLLED2
(
	sId number, 
	cID NUMBER,
	STATUS varchar2(15));

/*Tamal*/
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (1, 'Abdullah', 'Khulna', 0191111111, 'abdullah@gmail.com', '01-Jan-2000');
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (2, 'Misha', 'Khulna', 01922222222, 'misha@gmail.com', '02-Jan-2000');
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (3, 'Alam', 'Khulna', 01933333333, 'alam@gmail.com', '03-Jan-2000');
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (4, 'Biva', 'Khulna', 01944444444, 'biva@gmail.com', '04-Jan-2000');

/*Moyontee*/
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (5, 'Asif', 'Dhaka', 01811111111, 'asif@gmail.com', '01-Jan-2000');
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (6, 'Ahona', 'Dhaka', 01822222222, 'ahona@gmail.com', '02-Jan-2000');
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (7, 'Faiza', 'Dhaka', 01833333333, 'ashiq@gmail.com', '03-Jan-2000');
insert into Student(sId, sName, SAddress, sPhone, sEmail, sDateofBirth) VALUES (8, 'Ashiq', 'Dhaka', 01844444444, 'faiza@gmail.com', '04-Jan-2000');

insert into Course(cId, cName, courseNumber, Credit, TotalClass, tId) VALUES (1, 'Software Development', 1200, 1.5, 15, 1);
insert into Course(cId, cName, courseNumber, Credit, TotalClass, tId) VALUES (2, 'Discrete Mathematics', 1203, 3.0, 30, 1);
insert into Course(cId, cName, courseNumber, Credit, TotalClass, tId) VALUES (3, 'Object Oriented Programming', 1206, 3.0, 30, 1); 
insert into Course(cId, cName, courseNumber, Credit, TotalClass, tId) VALUES (4, 'Basic Electric Engineering', 1241, 1.5, 15, 2);
insert into Course(cId, cName, courseNumber, Credit, TotalClass, tId) VALUES (5, 'Mathematics 2', 1219, 3.0, 30, 2); 

insert into Teacher(tId, tName, tPhone, tEmail) VALUES (1, 'Anisul Islam', 01711111111, 'anisul@gmail.com');
insert into Teacher(tId, tName, tPhone, tEmail) VALUES (2, 'Kamrul Ahmed', 01911111111, 'kamrul@gmail.com');

insert into ENROLLED1(sId, cID, STATUS) VALUES (1, 1, 'ACCEPTED');
insert into ENROLLED1(sId, cID, STATUS) VALUES (2, 1, 'ACCEPTED');
insert into ENROLLED1(sId, cID, STATUS) VALUES (3, 1, 'ACCEPTED');
insert into ENROLLED1(sId, cID, STATUS) VALUES (4, 1, 'ACCEPTED');

select * from Student;
select * from Course;
select * from Teacher;
select * from ENROLLED1;
select * from ENROLLED2;
 
commit;
