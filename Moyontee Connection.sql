drop database link moyontee;

create database link moyontee
 connect to system identified by "12345"
 using '(DESCRIPTION =
       (ADDRESS_LIST =
         (ADDRESS = (PROTOCOL = TCP)
		 (HOST = 192.168.171.128)
		 (PORT = 1620))
       )
       (CONNECT_DATA =
         (SID = XE)
       )
     )'
;  
