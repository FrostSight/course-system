SET SERVEROUTPUT ON;
SET VERIFY OFF;

BEGIN
	DBMS_OUTPUT.PUT_LINE('CID' || CHR(9) || 'cName' || CHR(9) || 'courseNumber' || CHR(9) || 'Credit' || CHR(9) || 'TotalClass' || CHR(9) || 'tName');
	FOR J IN (SELECT * FROM Course NATURAL JOIN Teacher) LOOP
		DBMS_OUTPUT.PUT_LINE(J.CID || CHR(9) || J.cName || CHR(9) || J.courseNumber || CHR(9) || J.Credit || CHR(9) || J.TotalClass || CHR(9) || J.tName);
	END LOOP;

END;
/

ACCEPT A NUMBER PROMPT "STUDENT ID : "; 
ACCEPT B NUMBER PROMPT "COURSE ID : "; 

BEGIN
	insert into ENROLLED2(sId, cID, STATUS) VALUES (&A, &B, 'PENDING');
	DBMS_OUTPUT.PUT_LINE('YOUR REQUEST HAS BEEN SENT SUCCESSFULLY');
END;
/

