
# Course System

Course System is my 7th semester project. I built it using oracle PLSQL, which is a procedural language that extends SQL and runs on Oracle Database servers. It is a project that demonstrates the distributed database system. In this system, the database is decentralized and considered to be geographically dispersed.

Here a student will apply to enroll for a course to a course teacher. The enroll request will be stated as pending until approval comes from the course teacher. Both teacher and student have a dashboard to check the status. Teacher, student and course information are displayable separately.

The project uses PL/SQL blocks, which are units of code that can be compiled and executed by the PL/SQL engine. The blocks contain SQL statements for data manipulation, cursor control and transaction control, as well as PL/SQL specific constructs like loops, conditional branching and exception handling. The blocks are organized into packages, which are collections of related subprograms and variables.


## Features

- Distributed database system
- Decentralized and considered to be geographically dispersed
- Appeal for course
- Course request and approval premission
- Separate dash board for both teachers and students


## Screenshots
![image1](/uploads/c1ef4ccdacb1b8320a4fdc8efc2f2f24/image1.png)
![image2](/uploads/17a50616934f18c02b52842290d9c866/image2.png)
![image3](/uploads/707d72170340101d64a07dfadcfaf124/image3.png)


## Tech Used

**Language:** PLSQL 

**Server:** Oracle SQL Server


## Feedback

If you have any feedback, please reach out to at http://scr.im/84ta


